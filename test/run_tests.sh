#!/bin/bash
# This script will put the pyPCAZIP suite through its paces, exemplifying
# a lot of the most common ways it can be used.
function test {
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
        echo "FAILED" >&2
    else
        echo "PASSED" >&2
    fi
    return $status
}

mkdir -p output
rm -f output/*

echo -n "Test 1a - DCD format compression/decompression: "
pyPcazip -p 2ozq.pdb 2ozq.dcd  output/test1.pcz
pyPcaunzip output/test1.pcz  output/test1.dcd
test ./traj_check.py 2ozq.pdb output/test1.dcd reference/test1.dcd 0.05
echo -n "Test 1b - DCD format compression/decompression: "
pyPcazip -p 2ozq.pdb 2ozq.dcd output/test1.pcz -r 400
pyPcaunzip  output/test1.pcz  output/test1.dcd
test ./traj_check.py 2ozq.pdb output/test1.dcd reference/test1.dcd 0.03
